package ec.uio.picoyplaca.util;

import javax.annotation.ManagedBean;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

@ManagedBean
public class MessageUtils {
	
	public static void messageInfo(String id, String main, String desc) 
	{
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(id, new FacesMessage(FacesMessage.SEVERITY_INFO, main, desc));
	}

	public static void messageWarning(String id, String main, String desc) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(id, new FacesMessage(FacesMessage.SEVERITY_WARN, main, desc));
	}

	public static void messageError(String id, String main, String desc) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(id, new FacesMessage(FacesMessage.SEVERITY_ERROR, main, desc));
	}

	public static void messageFatal(String id, String main, String desc) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(id, new FacesMessage(FacesMessage.SEVERITY_FATAL, main, desc));
	}

}
