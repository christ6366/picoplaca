package ec.uio.picoyplaca.modelo;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class AutoDia implements Serializable {


	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long autdiIdentifi;

	@Basic(optional = false)
	@Size(min = 1, max = 8)
	private String placa;

	@Size(min = 1, max = 10)
	private String fecha;

	@Size(min = 1, max = 5)
	private String hora;

}
