package ec.uio.picoyplaca.administracion;

import javax.ejb.EJB;

import ec.uio.picoyplaca.modelo.AutoDia;
import ec.uio.picoyplaca.servicio.AutoDiaFacade;

public class IAdministracionImpl implements IAdministracionServicio {

	@EJB
	private AutoDiaFacade autoDiaFacade;
	
	@Override
	public AutoDia ppByPlaca(String placa) throws Exception {
		return autoDiaFacade.ppByPlaca(placa) ;
	}

}
