package ec.uio.picoyplaca.servicio;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import ec.uio.picoyplaca.modelo.AutoDia;

@Stateless
public class AutoDiaFacade extends AbstractFacade<AutoDia>  {
	private EntityManager em;

	public AutoDia ppByPlaca(String placa) throws Exception {
		AutoDia result = null;
		Criteria criteria = ((Session) getEntityManager().getDelegate()).createCriteria(AutoDia.class);
		criteria.add(Restrictions.eq("placa", placa));
		result = (AutoDia) criteria.uniqueResult();
		return result;
	}
	
	
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	public AutoDiaFacade() {
		super(AutoDia.class);
	}
	

}
